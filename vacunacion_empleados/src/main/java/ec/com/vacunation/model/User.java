package ec.com.vacunation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuario")
public class User implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_usuario", nullable = false)
    private Integer id_usuario;
    /**@Id
    @Column(name = "id_usuario", nullable = false)
    private Long id_usuario;*/
    @Column(name = "nombre_usuario", nullable = false)
    private String nombre_usuario;
    @Column(name = "ci_usuario", nullable = false)
    private String ci_usuario;
    @Column(name = "celular_usuario", nullable = false)
    private String celular_usuario;
    @Column(name = "usuario", nullable = false)
    private String usuario;
    @Column(name = "clave", nullable = false)
    private String clave;
    @Column(name = "tipo_usuario", nullable = false)
    private String tipo_usuario;
    @Column(name = "estado", nullable = false)
    private String estado;
	public final Integer getId_usuario() {
		return id_usuario;
	}
	public final void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public final String getNombre_usuario() {
		return nombre_usuario;
	}
	public final void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}
	public final String getCi_usuario() {
		return ci_usuario;
	}
	public final void setCi_usuario(String ci_usuario) {
		this.ci_usuario = ci_usuario;
	}
	public final String getCelular_usuario() {
		return celular_usuario;
	}
	public final void setCelular_usuario(String celular_usuario) {
		this.celular_usuario = celular_usuario;
	}
	public final String getUsuario() {
		return usuario;
	}
	public final void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public final String getClave() {
		return clave;
	}
	public final void setClave(String clave) {
		this.clave = clave;
	}
	public final String getTipo_usuario() {
		return tipo_usuario;
	}
	public final void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}
	public final String getEstado() {
		return estado;
	}
	public final void setEstado(String estado) {
		this.estado = estado;
	}
    
}
