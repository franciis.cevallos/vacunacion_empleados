package ec.com.vacunation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "empleado")
public class Empleado implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_empleado", nullable = false)
    private Integer id_empleado;
    
    @Column(name = "id_usuario", nullable = false)
    private Integer id_usuario;
    
    @Column(name = "cedula", nullable = false)
    private String cedula;
    
    @Column(name = "nombres_empleado", nullable = false)
    private String nombres_empleado;
    
    @Column(name = "apellido_empleado", nullable = false)
    private String apellido_empleado;
    
    @Column(name = "correo", nullable = false)
    private String correo;
    
    @Column(name = "fecha_nacimiento", nullable = false)
    private String fecha_nacimiento;
    
    @Column(name = "direccion", nullable = false)
    private String direccion;
    
    @Column(name = "telefono", nullable = false)
    private String telefono;
    
    @Column(name = "estado_vacunacion", nullable = false)
    private String estado_vacunacion;
    
    @Column(name = "estado", nullable = false)
    private String estado;

	public final Integer getId_empleado() {
		return id_empleado;
	}

	public final void setId_empleado(Integer id_empleado) {
		this.id_empleado = id_empleado;
	}

	public final Integer getId_usuario() {
		return id_usuario;
	}

	public final void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	public final String getCedula() {
		return cedula;
	}

	public final void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public final String getNombres_empleado() {
		return nombres_empleado;
	}

	public final void setNombres_empleado(String nombres_empleado) {
		this.nombres_empleado = nombres_empleado;
	}

	public final String getApellido_empleado() {
		return apellido_empleado;
	}

	public final void setApellido_empleado(String apellido_empleado) {
		this.apellido_empleado = apellido_empleado;
	}

	public final String getCorreo() {
		return correo;
	}

	public final void setCorreo(String correo) {
		this.correo = correo;
	}

	public final String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public final void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public final String getDireccion() {
		return direccion;
	}

	public final void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public final String getTelefono() {
		return telefono;
	}

	public final void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public final String getEstado_vacunacion() {
		return estado_vacunacion;
	}

	public final void setEstado_vacunacion(String estado_vacunacion) {
		this.estado_vacunacion = estado_vacunacion;
	}

	public final String getEstado() {
		return estado;
	}

	public final void setEstado(String estado) {
		this.estado = estado;
	}
	
    
}
