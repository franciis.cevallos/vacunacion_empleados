package ec.com.vacunation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "vacunas")
public class Vacunas implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_vacunas", nullable = false)
    private Integer id_vacunas;
   
    @Column(name = "id_empleado", nullable = false)
    private Integer id_empleado;
    
    @Column(name = "tipo_vacuna", nullable = false)
    private String tipo_vacuna;
    
    @Column(name = "celular_usuario", nullable = false)
    private String celular_usuario;
    
    @Column(name = "fecha_vacunacion", nullable = false)
    private String fecha_vacunacion;
    
    @Column(name = "numero_diosis", nullable = false)
    private Integer numero_diosis;
    
    @Column(name = "estado", nullable = false)
    private String estado;

	public final Integer getId_vacunas() {
		return id_vacunas;
	}

	public final void setId_vacunas(Integer id_vacunas) {
		this.id_vacunas = id_vacunas;
	}

	public final Integer getId_empleado() {
		return id_empleado;
	}

	public final void setId_empleado(Integer id_empleado) {
		this.id_empleado = id_empleado;
	}

	public final String getTipo_vacuna() {
		return tipo_vacuna;
	}

	public final void setTipo_vacuna(String tipo_vacuna) {
		this.tipo_vacuna = tipo_vacuna;
	}

	public final String getCelular_usuario() {
		return celular_usuario;
	}

	public final void setCelular_usuario(String celular_usuario) {
		this.celular_usuario = celular_usuario;
	}

	public final String getFecha_vacunacion() {
		return fecha_vacunacion;
	}

	public final void setFecha_vacunacion(String fecha_vacunacion) {
		this.fecha_vacunacion = fecha_vacunacion;
	}

	public final Integer getNumero_diosis() {
		return numero_diosis;
	}

	public final void setNumero_diosis(Integer numero_diosis) {
		this.numero_diosis = numero_diosis;
	}

	public final String getEstado() {
		return estado;
	}

	public final void setEstado(String estado) {
		this.estado = estado;
	}
    
    
    
    
}
