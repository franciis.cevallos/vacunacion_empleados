package ec.com.vacunation.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ec.com.vacunation.model.Empleado;
import ec.com.vacunation.services.EmpleadoServices;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/emapleado")
@RequiredArgsConstructor
public class EmpleadoController {
	@Autowired
	private EmpleadoServices empleadoServices;

	
	@GetMapping("/getAllusers")
	public List<Empleado> getAllusers() {
		List<Empleado> empleados = empleadoServices.findAll();
		for (Empleado empleado : empleados) {
			System.out.println(empleado.getNombres_empleado());
		}
	    return empleados;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Empleado> getUserById(@PathVariable("id") Integer id) {
	
	    return new ResponseEntity<>(empleadoServices.findById(UUID.fromString(id.toString())), HttpStatus.OK);
	}
	
	@PostMapping()
	public ResponseEntity<Empleado> createEmpleado(@RequestBody Empleado user) {
	
	    return new ResponseEntity<>(empleadoServices.saveUser(user), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Empleado> updateEmpleado(@PathVariable("id") String id, @RequestBody Empleado empleado) {
	
	    if (empleadoServices.existById(UUID.fromString(id))) {
	        return new ResponseEntity<>(empleadoServices.updateEmpleado(empleado), HttpStatus.ACCEPTED);
	    }
	
	    throw new IllegalArgumentException("Emplado with id " + id + "not found");
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteEmpleado(@PathVariable("id") String id) {
	
		empleadoServices.deleteEmpleado(UUID.fromString(id));
	
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
