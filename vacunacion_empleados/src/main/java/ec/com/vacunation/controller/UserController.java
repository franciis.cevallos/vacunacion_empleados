package ec.com.vacunation.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ec.com.vacunation.model.User;
import ec.com.vacunation.services.UserServices;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
	@Autowired
	private UserServices userServices;

	
	@GetMapping("/getAllusers")
	public List<User> getAllusers() {
		List<User> users = userServices.findAll();
		for (User user : users) {
			System.out.println(user.getNombre_usuario());
		}
	    return users;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") Integer id) {
	
	    return new ResponseEntity<>(userServices.findById(UUID.fromString(id.toString())), HttpStatus.OK);
	}
	
	@PostMapping()
	public ResponseEntity<User> createUser(@RequestBody User user) {
	
	    return new ResponseEntity<>(userServices.saveUser(user), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody User user) {
	
	    if (userServices.existById(UUID.fromString(id))) {
	        return new ResponseEntity<>(userServices.saveUser(user), HttpStatus.ACCEPTED);
	    }
	
	    throw new IllegalArgumentException("User with id " + id + "not found");
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") String id) {
	
		userServices.deleteUser(UUID.fromString(id));
	
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
