package ec.com.vacunation.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ec.com.vacunation.model.Vacunas;
import ec.com.vacunation.services.VacunasServices;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/vacunas")
@RequiredArgsConstructor
public class VacunasController {
	@Autowired
	private VacunasServices vacunasServices;

	
	@GetMapping("/getAllvacunas")
	public List<Vacunas> getAllusers() {
		List<Vacunas> vacunas = vacunasServices.findAll();
		for (Vacunas vacuna : vacunas) {
			System.out.println(vacuna.getTipo_vacuna());
		}
	    return vacunas;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Vacunas> getVacunasById(@PathVariable("id") Integer id) {
	
	    return new ResponseEntity<>(vacunasServices.findById(UUID.fromString(id.toString())), HttpStatus.OK);
	}
	
	@PostMapping()
	public ResponseEntity<Vacunas> createVacunas(@RequestBody Vacunas vacunas) {
	
	    return new ResponseEntity<>(vacunasServices.saveVacunas(vacunas), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Vacunas> updateUser(@PathVariable("id") String id, @RequestBody Vacunas vacunas) {
	
	    if (vacunasServices.existById(UUID.fromString(id))) {
	        return new ResponseEntity<>(vacunasServices.saveVacunas(vacunas), HttpStatus.ACCEPTED);
	    }
	
	    throw new IllegalArgumentException("Vacunas with id " + id + "not found");
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteVacunas(@PathVariable("id") String id) {
	
		vacunasServices.deleteVacunas(UUID.fromString(id));
	
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
