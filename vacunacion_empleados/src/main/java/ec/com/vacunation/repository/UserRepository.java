package ec.com.vacunation.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.com.vacunation.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

}
