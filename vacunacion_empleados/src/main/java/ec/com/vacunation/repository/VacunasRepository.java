package ec.com.vacunation.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.com.vacunation.model.Vacunas;

@Repository
public interface VacunasRepository extends JpaRepository<Vacunas, UUID> {

}
