package ec.com.vacunation.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.vacunation.model.User;
import ec.com.vacunation.repository.UserRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class UserServices {
	
	@Autowired
	private UserRepository userRepository;

    public List<User> findAll() {

        return userRepository.findAll();
    }

    public User findById(UUID uuid) {
        return userRepository.findById(uuid).orElseThrow(() -> new NoSuchElementException());
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUser(UUID uuid) {

    	userRepository.deleteById(uuid);
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public boolean existById(UUID uuid) {
        return userRepository.existsById(uuid);
    }
}
