package ec.com.vacunation.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.vacunation.model.Empleado;
import ec.com.vacunation.repository.EmpleadoRepository;
import ec.com.vacunation.repository.UserRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class EmpleadoServices {
	
	@Autowired
	private EmpleadoRepository empladoRepository;

    public List<Empleado> findAll() {

        return empladoRepository.findAll();
    }

    public Empleado findById(UUID uuid) {
        return empladoRepository.findById(uuid).orElseThrow(() -> new NoSuchElementException());
    }

    public Empleado saveUser(Empleado empleado) {
        return empladoRepository.save(empleado);
    }

    public void deleteEmpleado(UUID uuid) {

    	empladoRepository.deleteById(uuid);
    }

    public Empleado updateEmpleado(Empleado user) {
        return empladoRepository.save(user);
    }

    public boolean existById(UUID uuid) {
        return empladoRepository.existsById(uuid);
    }
}
