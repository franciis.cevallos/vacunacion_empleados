package ec.com.vacunation.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.vacunation.model.User;
import ec.com.vacunation.model.Vacunas;
import ec.com.vacunation.repository.UserRepository;
import ec.com.vacunation.repository.VacunasRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class VacunasServices {
	
	@Autowired
	private VacunasRepository vacunasRepository;

    public List<Vacunas> findAll() {

        return vacunasRepository.findAll();
    }

    public Vacunas findById(UUID uuid) {
        return vacunasRepository.findById(uuid).orElseThrow(() -> new NoSuchElementException());
    }

    public Vacunas saveVacunas(Vacunas vacunas) {
        return vacunasRepository.save(vacunas);
    }

    public void deleteVacunas(UUID uuid) {

    	vacunasRepository.deleteById(uuid);
    }

    public Vacunas updateVacunas(Vacunas vacunas) {
        return vacunasRepository.save(vacunas);
    }

    public boolean existById(UUID uuid) {
        return vacunasRepository.existsById(uuid);
    }
}
